(function (window, document) {
  'use strict';

  window.onload = function (onloadEvent) {
    window.addEventListener('message', function (e) {
      if (typeof e.data === 'object' && e.data.length > 1) {
        var iframe = document.getElementById('veplatform-iframe');
        if (typeof iframe !== 'object') {
          return;
        }
        if (e.data[0] === 'setHeight') {
          var data = parseInt(e.data[1]);
          iframe.style.height = data + 'px';
        }
        else if (e.data[0] === 'doScroll') {
          var position = parseInt(e.data[1]);
          if (typeof iframe.offsetParent === 'object') {
            position += parseInt(iframe.offsetParent.offsetTop);
          }
          veAnimate(document.documentElement, 'scrollTop', '', document.documentElement.scrollTop, position, 300, true);
          veAnimate(document.body, 'scrollTop', '', document.body.scrollTop, position, 300, true);
        }
      }
    }, false);
  };

  function veAnimate(element, style, unit, from, to, time, prop) {
    if (!element) {
      return;
    }
    var start = new Date().getTime();
    var timer = setInterval(function () {
      var step = Math.min(1, (new Date().getTime() - start) / time);
      if (prop) {
        element[style] = (from + step * (to - from)) + unit;
      }
      else {
        element.style[style] = (from + step * (to - from)) + unit;
      }
      if (step === 1) {
        clearInterval(timer);
      }
    }, 25);
    element.style[style] = from + unit;
  }

}(window, document));
