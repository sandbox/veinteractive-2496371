<?php
/**
 * VeAPI
 */

require_once __DIR__ . '/veplatform_veplatformapi.inc';

class CommerceVeplatformVeAPI extends CommerceVePlatformAPI {
  protected $requestEcommerce = 'Drupal/';

  /**
   * Get tag, pixel, token from configuration
   */
  protected function loadConfig() {
    $config = variable_get('veplatform_config', array());
    $this->config['tag'] = array_key_exists('ve_tag', $config) ? $config['ve_tag'] : $this->config['tag'];
    $this->config['pixel'] = array_key_exists('ve_pixel', $config) ? $config['ve_pixel'] : $this->config['pixel'];
    $this->config['token'] = array_key_exists('ve_token', $config) ? $config['ve_token'] : $this->config['token'];
  }

  /**
   * Store tag, pixel and journey code in the DB
   * @param string $journey
   *
   * @return bool
   */
  protected function saveJourney($journey) {
    $config = array(
      've_tag' => $journey->URLTag,
      've_pixel' => $journey->URLPixel,
      've_token' => $journey->Token,
    );
    variable_set('veplatform_config', $config);
    return TRUE;
  }

  /**
   * Delete from config
   */
  protected function deleteConfig() {
    variable_del('veplatform_config');
  }

  /**
   * Set required params for iframe
   */
  protected function setParams() {
    global $base_url, $language, $conf;

    $domain = preg_replace("(^https?:\/\/)", "", $base_url);
    $currency = empty($conf['commerce_default_currency']) ? variable_get('commerce_default_currency', 'USD') : $conf['commerce_default_currency'];
    $country = empty($conf['site_default_country']) ? 'GB' : $conf['site_default_country'];
    $isInstallFlow = "false";

    $this->requestParams = array(
      'domain' => $domain,
      'language' => $language->language,
      'email' => $conf['site_mail'],
      'phone' => NULL,
      'merchant' => $conf['site_name'],
      'country' => $country,
      'currency' => $currency,
      'isInstallFlow' => $isInstallFlow,
    );
  }

  /**
   * Get the iframe url
   * @return string
   */
  public function getIframe() {
    $url = $this->iframe . '?ecommerce=Drupal&token=' . urlencode($this->config['token']);
    foreach ($this->requestParams as $key => $value) {
      $url .= '&' . $key . '=' . urlencode($value);
    }
    return $url;
  }

}
