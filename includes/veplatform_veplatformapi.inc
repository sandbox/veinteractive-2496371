<?php

/**
 * VePlatformAPI requests
 */
abstract class CommerceVePlatformAPI {
  protected $requestDomain = 'http://veconnect.veinteractive.com/API/';
  protected $requestEcommerce = 'EcommerceIdentifier/';
  protected $requestMerchant = 'GetMerchantInfo';
  protected $requestInstall = 'Install';
  protected $requestUninstall = 'Uninstall';
  protected $requestProducts = 'ActivateProducts';
  protected $requestTimeout = 15;
  protected $veProducts = array(
    'vecontact' => 1,
    'vechat' => 2,
    'veassist' => 3,
    'veads' => 5,
  );
  protected $requestParams = array();
  protected $config = array(
    'tag' => NULL,
    'pixel' => NULL,
    'token' => NULL,
    'products' => array()
  );
  protected $iframe = 'http://veconnect.veinteractive.com/Veframe/';

  public function __construct() {
    $this->setParams();
    $this->loadConfig();
  }

  abstract protected function setParams();

  abstract protected function loadConfig();

  abstract protected function saveJourney($journey);

  abstract protected function deleteConfig();

  /**
   * Get Token
   * @return bool
   */
  protected function getToken() {
    $token = $this->getConfigOption('token');
    return $token;
  }

  /**
   * Retrieving configuration data
   * @param string $option
   * @param bool $reload (default: FALSE)
   * @return string
   */
  public function getConfigOption($option, $reload = FALSE) {
    if($reload === TRUE) {
      $this->loadConfig();
    }
    $value = array_key_exists($option, $this->config) ? $this->config[$option] : NULL;
    return $value;
  }

  /**
   * Check if VePlatform is installed
   * @return bool
   */
  public function isInstalled() {
    foreach (array('tag', 'pixel', 'token') as $name) {
      if($this->config[$name] === NULL) {
        return FALSE;
      }
    }
    return TRUE;
  }

  /**
   * Show login
   * @return bool
   */
  public function showLogin() {
    $response = $this->isInstalled() && count($this->config['products']) > 0;
    return $response;
  }

  /**
   * Check if any product is active
   * @param string $product
   * @return bool
   */
  public function isProductActive($product) {
    $response = array_key_exists($product, $this->veProducts) &&
      in_array($this->veProducts[$product], $this->config['products']);
    return $response;
  }

  /**
   * Install the module
   * @return bool
   */
  public function installModule() {
    $params = $this->requestParams;
    $response = $this->getRequest($this->requestInstall, $params);
    if($response) {
      $journey = json_decode($response);
      if(isset($journey->URLPixel) && isset($journey->URLTag) && isset($journey->Token)) {
        $journey->URLPixel = $this->cleanUrl($journey->URLPixel);
        $journey->URLTag = $this->cleanUrl($journey->URLTag);
        return $this->saveJourney($journey);
      }
    }
    return FALSE;
  }

  /**
   * Remove protocol from url
   * @param string $url
   * @return string
   */
  protected function cleanUrl($url) {
    $cleanUrl = preg_replace("(^https?:)", "", $url);
    return $cleanUrl;
  }

  /**
   * Uninstall the module
   * @return bool
   */
  public function uninstallModule() {
    $params = $this->requestParams;
    $params['token'] = $this->getToken();
    $this->deleteConfig();
    $response = $this->getRequest($this->requestUninstall, $params);
    if($response) {
      return json_decode($response);
    }
    return FALSE;
  }

  /**
   * Activate the products
   * @param array $productsForm
   * @return bool
   */
  public function activateProducts($productsForm) {
    $products = array();
    foreach ($productsForm as $product) {
      if(array_key_exists($product, $this->veProducts)) {
        $products[] = $this->veProducts[$product];
      }
    }
    if(count($products) === 0) {
      return true;
    }
    $params = $this->requestParams;
    $params['token'] = $this->getToken();
    $params['taskId'] = 1;
    $params['appCodes'] = implode('|', $products);
    $response = $this->getRequest($this->requestProducts, $params);
    if($response) {
      $response = json_decode($response);
      if($response === TRUE) {
        $oldProducts = $this->getConfigOption('products');
        $productsToSave = array_merge($oldProducts, $products);
        $productsToSave = array_unique($productsToSave);
        sort($productsToSave);
        return TRUE;
      }
    }
    return FALSE;
  }

  /**
   * CURL request to web service
   * @param string $requestAction
   * @param array $params
   * @return mixed
   */
  protected function getRequest($requestAction, $params) {
    $params = json_encode($params);
    $ch = curl_init($this->requestDomain . $this->requestEcommerce . $requestAction);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
      'Content-Type: application/json',
      'Content-Length: ' . drupal_strlen($params),
    ));
    $response = curl_exec($ch);
    return $response;
  }

}
